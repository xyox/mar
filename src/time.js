import { svelteTime } from 'svelte-time';
import 'dayjs/esm/locale/es';
import dayjs from 'dayjs/esm';
dayjs.locale('es');
export default svelteTime;
export { dayjs };
