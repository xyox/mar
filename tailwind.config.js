module.exports = {
    // important: true,
    purge: {
        enabled: !process.env.ROLLUP_WATCH,
        mode: 'all',
        content: ['./**/**/*.html', './**/**/*.svelte'],

        options: {
            whitelistPatterns: [/svelte-/],
            defaultExtractor: content =>
                [...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(
                    // eslint-disable-next-line no-unused-vars
                    ([_match, group, ..._rest]) => group,
                ),
        },
    },
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            fontFamily: {
                header: ["'KoHo'", 'sans-serif'],
            },
            // gradientColorStops: () => ({
            //     primary: '#FF8C00',
            //     secondary: '#FFA500',
            //     danger: '#FFD700',
            // }),
            colors: {
                // 'test-blue': {
                //     100: '#d0e7fb',
                //     DEFAULT: '#0063bd',
                //     500: '#4299e1',
                //     700: '#2b6cb0',
                // },
                orange: {
                    100: '#fffaf0',
                    200: '#feebc8',
                    300: '#fbd38d',
                    400: '#f6ad55',
                    500: '#ed8936',
                    600: '#dd6b20',
                    700: '#c05621',
                    800: '#9c4221',
                    900: '#7b341e',
                },
            },
        },
    },
    variants: {
        extend: {
            lineClamp: ['hover'],
        },
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/line-clamp'),
    ],
};
